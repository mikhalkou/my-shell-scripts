#!/bin/bash

# Converts files in a folder  windows-1251 -> utf-8 recursively

find ./ -name "*.txt" -o -name "*.java" -o -name "*.csv" -type f |
while read file
do
  mv $file $file._tmp
  iconv -f WINDOWS-1251 -t UTF-8 $file._tmp > $file
  rm -f $file._tmp
  echo "$file converted."
done
